//
//  BaseViewController.swift
//  NaviaLife
//
//  Created by Shailendra on 17/09/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var delegate = AppDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        delegate = UIApplication.shared.delegate as! AppDelegate
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func ShowAlert(MsgTitle:String){
        let actionSheetController: UIAlertController = UIAlertController(title: "", message: "\(MsgTitle)", preferredStyle: .alert)
        let OKAction: UIAlertAction = UIAlertAction(title:"OK", style: .default) { action -> Void in
        }
        actionSheetController.addAction(OKAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func showLoader(message: String){
        DispatchQueue.main.async {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.label.text = message
        }
    }
    func hideLoader(){
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}
