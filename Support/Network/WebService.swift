import UIKit

@objc protocol WebServiceProtocal {
    @objc optional func returnSuccess(paraDict:NSDictionary,code: Int)
    @objc optional func returnSuccessTypeArray(paraDict:NSArray,code: Int)
    @objc optional func returnFailureDesc(desc: String)
    @objc optional func returnFail()
}

public class WebServiceCall: NSObject,NSURLConnectionDataDelegate{

    var responseData: NSMutableData = NSMutableData()
    var delegate: WebServiceProtocal?
    var request : NSMutableURLRequest = NSMutableURLRequest()
    var Code : Int?
    var HttpBody : NSString!
    var urlPath : String = ""
    var delegates = UIApplication.shared.delegate as! AppDelegate
  
    

    func UrlWithConnection(urlPath:String,MethodCode : String){
        let url: URL = URL(string: urlPath as String)!
        self.request = NSMutableURLRequest(url: url)
        self.request.httpMethod = MethodCode
        if(HttpBody != ""){
          self.request.httpBody = self.HttpBody.data(using:  String.Encoding.utf8.rawValue)
        }
        self.request.addValue(kContentTypeValueKey, forHTTPHeaderField: kContentTypeKey)
        let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self,
                                                          startImmediately: false)!
        connection.schedule(in:.main, forMode: .defaultRunLoopMode)
        connection.start()
    }
    public func connection(_ connection: NSURLConnection, didFailWithError error: Error) {

    }
    public func connection(_ didReceiveResponse: NSURLConnection, didReceive response: URLResponse) {
        let httpResponse = response as! HTTPURLResponse
        self.Code = httpResponse.statusCode
        self.responseData = NSMutableData()
    }
    public func connection(_ connection: NSURLConnection, didReceive data: Data) {
        self.responseData.append(data)
    }
   public func connectionDidFinishLoading(_ connection: NSURLConnection) {
        do {
            if let paraDict = try JSONSerialization.jsonObject(with: self.responseData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary{
                self.delegate?.returnSuccess!(paraDict: paraDict, code: self.Code!)
            }
            else{
                self.delegate?.returnFail!()
            }
        }
        catch{
            self.delegate?.returnFail!()
        }
    }

    // GetDiet
    func GetDietWebApi(){
        self.urlPath = kBaseURLString + kDummyWebApi
        self.HttpBody = ""
        print("HttpBody=====\(HttpBody!)")
        self.UrlWithConnection(urlPath: self.urlPath, MethodCode: kGET)
    }
}
