//
//  Constant.swift
//  NaviaLife
//
//  Created by Shailendra on 17/09/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import Foundation

//MARK:- API Keys

// Basic
let kContentTypeKey            = "Content-Type"
let kContentTypeValueKey       = "application/json"



let kDummyWebApi		       = "dummy/"


var kPOST = "POST"
var kPUT  = "PUT"
var kGET  = "GET"

var kBaseURLString = "https://naviadoctors.com/"


