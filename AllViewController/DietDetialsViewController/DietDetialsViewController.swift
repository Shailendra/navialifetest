//
//  NaviaLifeDetialsViewController.swift
//  NaviaLife
//
//  Created by Shailendra on 17/09/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import UIKit

class DietDetialsViewController: BaseViewController {

    @IBOutlet weak var FoodNameLabel: UILabel!
    @IBOutlet weak var FoodTimeLabel: UILabel!
    @IBOutlet weak var DaysLabel: UILabel!
    
    var FoodNameStr : String! = ""
    var FoodTimeStr : String! = ""
    var DaysStr : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.FoodNameLabel.text = "Food Name: \(FoodNameStr!)"
        self.FoodTimeLabel.text = "Food Time: \(FoodTimeStr!)"
        self.DaysLabel.text = "\(DaysStr!)"
    }
    
    @IBAction func BackAction(){
        self.dismiss(animated: true, completion: nil)
    }
}
