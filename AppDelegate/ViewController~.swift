//
//  ViewController.swift
//  DeviceInfo
//
//  Created by Shailendra on 16/08/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import UIKit

class ViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource  {
    
    @IBOutlet var DeviceInfoTableView : UITableView!
    var TitleArray : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DeviceInfoTableView.estimatedRowHeight = 150;
        self.DeviceInfoTableView.rowHeight = UITableViewAutomaticDimension;
        self.DeviceInfoTableView.register(UINib(nibName: "DeviceCell", bundle: nil), forCellReuseIdentifier: "DeviceCell")
        
        self.TitleArray.append("Phone Details")
        self.TitleArray.append("Contact List")
        self.TitleArray.append("Call Log List")
        self.TitleArray.append("Install Apps List")
        self.TitleArray.append("Browsing History")
        self.TitleArray.append("Current Wallpaper")
        self.TitleArray.append("App Usages")
        self.TitleArray.append("SMS Read")
        self.TitleArray.append("GPS Locations")
        self.TitleArray.append("Battery Usages")
        self.TitleArray.append("How long using iPhone")
    }
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TitleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : DeviceCell = tableView.dequeueReusableCell(
            withIdentifier: "DeviceCell", for: indexPath) as! DeviceCell
        Cell.TitleLabel.text = "\(TitleArray[indexPath.row])"
        Cell.tag  = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(sender:)))
        Cell.addGestureRecognizer(tapGesture)
        return Cell
    }
    @objc func tapBlurButton(sender: UITapGestureRecognizer) {
        let TagValue = sender.view?.tag
        if(TagValue == 0){
            let ObjPhoneDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "PhoneDetailsViewController") as! PhoneDetailsViewController
            self.navigationController?.pushViewController(ObjPhoneDetailsViewController, animated: true)
        }
        else if(TagValue == 1){
            let ObjContactViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            self.navigationController?.pushViewController(ObjContactViewController, animated: true)
        }
        else if(TagValue == 2){
            let ObjCallLogViewController = self.storyboard?.instantiateViewController(withIdentifier: "CallLogViewController") as! CallLogViewController
            self.navigationController?.pushViewController(ObjCallLogViewController, animated: true)
        }
        else if(TagValue == 3){
            let ObjInstallAppListViewController = self.storyboard?.instantiateViewController(withIdentifier: "InstallAppListViewController") as! InstallAppListViewController
            self.navigationController?.pushViewController(ObjInstallAppListViewController, animated: true)
        }
        else if(TagValue == 4){
            let ObjBrowserHistoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "BrowserHistoryViewController") as! BrowserHistoryViewController
            self.navigationController?.pushViewController(ObjBrowserHistoryViewController, animated: true)
        }
        else if(TagValue == 5){
            let ObjWallpaperViewController = self.storyboard?.instantiateViewController(withIdentifier: "WallpaperViewController") as! WallpaperViewController
            self.navigationController?.pushViewController(ObjWallpaperViewController, animated: true)
        }
        else if(TagValue == 6){
            let ObjAppUsagesViewController = self.storyboard?.instantiateViewController(withIdentifier: "AppUsagesViewController") as! AppUsagesViewController
            self.navigationController?.pushViewController(ObjAppUsagesViewController, animated: true)
        }
        else if(TagValue == 7){
            let ObjSMSReadViewController = self.storyboard?.instantiateViewController(withIdentifier: "SMSReadViewController") as! SMSReadViewController
            self.navigationController?.pushViewController(ObjSMSReadViewController, animated: true)
        }
        else if(TagValue == 8){
            let ObjGPSViewController = self.storyboard?.instantiateViewController(withIdentifier: "GPSViewController") as! GPSViewController
            self.navigationController?.pushViewController(ObjGPSViewController, animated: true)
        }
        else if(TagValue == 9){
            let ObjBattertyViewController = self.storyboard?.instantiateViewController(withIdentifier: "BattertyViewController") as! BattertyViewController
            self.navigationController?.pushViewController(ObjBattertyViewController, animated: true)
        }
        else if(TagValue == 10){
            let ObjiPhoneUsingViewController = self.storyboard?.instantiateViewController(withIdentifier: "iPhoneUsingViewController") as! iPhoneUsingViewController
            self.navigationController?.pushViewController(ObjiPhoneUsingViewController, animated: true)
        }
    }
}

