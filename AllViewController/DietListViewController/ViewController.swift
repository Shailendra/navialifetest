//
//  ViewController.swift
//  NaviaLife
//
//  Created by Shailendra on 17/09/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    @IBOutlet var DietTableView : UITableView!
    var FoodNameArray : [String] = []
    var FoodTimeArray : [String] = []
    var DietDaysArray : [String] = []
    
    let API                                     = WebServiceCall()
    var ResponseDic = NSDictionary()
    var mondayArray = NSArray()
    var thursdayArray = NSArray()
    var wednesdayArray = NSArray()
    var CheckValue : String! = ""
    let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CheckValue = ""
        self.API.delegate = self
        self.DietTableView.estimatedRowHeight = 150;
        self.DietTableView.rowHeight = UITableViewAutomaticDimension;
        self.DietTableView.register(UINib(nibName: "DietCell", bundle: nil), forCellReuseIdentifier: "DietCell")
        self.refreshControl.addTarget(self, action: #selector(ViewController.refresh), for: UIControlEvents.valueChanged)
        self.DietTableView.addSubview(self.refreshControl)
        self.perform(#selector(ViewController.GetDietWebService), with: nil, afterDelay: 0.5)

    }
    @objc func refresh(){
        self.CheckValue = "REFRESH"
        self.perform(#selector(ViewController.GetDietWebService), with: nil, afterDelay: 0.5)
    }
    @objc func GetDietWebService(){
        if(Reachability.isConnectedToNetwork()){
            if(self.CheckValue != "REFRESH"){
               self.showLoader(message: "Loading")
            }
            self.API.GetDietWebApi()
        }
        else{
            self.ShowAlert(MsgTitle:"Please check your Internet Connection")
        }
    }
    func SetNotification(){
        var dateNotification = Date()
        for i in 0..<FoodTimeArray.count {
            let CombineDate = "\(DietDaysArray[i]), \(FoodTimeArray[i])"
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE, HH:mm"
            formatter.timeZone = NSTimeZone.default
            dateNotification = formatter.date(from: CombineDate)!
//            let dayName = formatter.string(from: dateNotification.addingTimeInterval(-5.0*60))
            localeNotification(dateNotification.addingTimeInterval(-5.0*60), index: i)
        }
    }
    func localeNotification(_ dateNotification: Date?, index Index: Int) {
        var userInfo: [AnyHashable : Any] = [:]
        userInfo["FOODDAYS"] = "\(DietDaysArray[Index])"
        userInfo["FOODNAME"] = "\(FoodNameArray[Index])"
        userInfo["FOODTIME"] = "\(FoodTimeArray[Index])"
       
        let localNotif = UILocalNotification()
        localNotif.fireDate = dateNotification
        localNotif.alertTitle = "Diet Notification"
        localNotif.alertBody = "\(FoodNameArray[Index])"
        localNotif.soundName = UILocalNotificationDefaultSoundName
        localNotif.repeatInterval = .NSDayCalendarUnit
        localNotif.applicationIconBadgeNumber = 1
        localNotif.userInfo = userInfo
        UIApplication.shared.scheduleLocalNotification(localNotif)
    }
}

// MARK: - WebService Delegate
extension ViewController : WebServiceProtocal {
    func returnSuccess(paraDict: NSDictionary, code: Int) {
        self.refreshControl.endRefreshing()
         self.CheckValue = ""
        print("paraDict====\(paraDict)")
        self.ResponseDic = paraDict.value(forKey: "week_diet_data") as! NSDictionary
        self.mondayArray = self.ResponseDic.value(forKey: "monday") as! NSArray
        self.wednesdayArray = self.ResponseDic.value(forKey: "wednesday") as! NSArray
        self.thursdayArray = self.ResponseDic.value(forKey: "thursday") as! NSArray
        
        for i in 0 ..< mondayArray.count {
            let Dic = mondayArray[i] as! NSDictionary
            self.DietDaysArray.append("Monday")
            self.FoodNameArray.append("\(Dic.value(forKey: "food") as! String)")
            self.FoodTimeArray.append("\(Dic.value(forKey: "meal_time") as! String)")
        }
        for i in 0 ..< wednesdayArray.count {
            let Dic = wednesdayArray[i] as! NSDictionary
            self.DietDaysArray.append("Wednesday")
            self.FoodNameArray.append("\(Dic.value(forKey: "food") as! String)")
            self.FoodTimeArray.append("\(Dic.value(forKey: "meal_time") as! String)")
        }
        for i in 0 ..< thursdayArray.count {
            let Dic = thursdayArray[i] as! NSDictionary
            self.DietDaysArray.append("Thursday")
            self.FoodNameArray.append("\(Dic.value(forKey: "food") as! String)")
            self.FoodTimeArray.append("\(Dic.value(forKey: "meal_time") as! String)")
        }
        self.SetNotification()
        self.hideLoader()
        self.DietTableView.reloadData()
    }
    func returnFail() {
        self.CheckValue = ""
        self.hideLoader()
    }
    func returnFailureDesc(desc: String) {
        self.hideLoader()
         self.CheckValue = ""
        self.ShowAlert(MsgTitle: "\(desc)")
    }
}
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FoodNameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell : DietCell = tableView.dequeueReusableCell(
            withIdentifier: "DietCell", for: indexPath) as! DietCell
        Cell.FoodNameLabel.text = "\(FoodNameArray[indexPath.row])"
        Cell.DietDayLabel.text = "\(DietDaysArray[indexPath.row])"
        Cell.tag  = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(sender:)))
        Cell.addGestureRecognizer(tapGesture)
        return Cell
    }
    @objc func tapBlurButton(sender: UITapGestureRecognizer) {
        let TagValue = sender.view?.tag
        let ViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DietDetialsViewController") as! DietDetialsViewController
        ViewController.FoodNameStr = "\(FoodNameArray[TagValue!])"
        ViewController.FoodTimeStr = "\(FoodTimeArray[TagValue!])"
        ViewController.DaysStr = "\(DietDaysArray[TagValue!])"
        ViewController.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.present(ViewController, animated: true, completion: nil)
    }
}
