//
//  RemiderViewController.swift
//  NaviaLife
//
//  Created by Shailendra on 17/09/18.
//  Copyright © 2018 Spice. All rights reserved.
//

import UIKit

class RemiderViewController: BaseViewController {
    
    @IBOutlet weak var ReminderFoodLabel: UILabel!
    @IBOutlet weak var DietTimeLabel: UILabel!
    @IBOutlet weak var DaysLabel: UILabel!
    var NotiDic = NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ReminderFoodLabel.text = "Reminder Food: \(NotiDic.value(forKey: "FOODNAME") as! String)"
        self.DietTimeLabel.text = "Diet Time: \(NotiDic.value(forKey: "FOODTIME") as! String)"
        self.DaysLabel.text = "\(NotiDic.value(forKey: "FOODDAYS") as! String)"
    }
    @IBAction func BackAction(){
        self.delegate.DietListPage()
    }
}
